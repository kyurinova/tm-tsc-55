package ru.tsc.kyurinova.tm.exception.user;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! E-mail already exists...");
    }

}
