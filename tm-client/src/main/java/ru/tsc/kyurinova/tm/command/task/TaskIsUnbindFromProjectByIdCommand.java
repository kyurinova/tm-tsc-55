package ru.tsc.kyurinova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.endpoint.ProjectDTO;
import ru.tsc.kyurinova.tm.endpoint.TaskDTO;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class TaskIsUnbindFromProjectByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-unbind-from-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project by id...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = sessionService.getSession();
        ;
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findByIdProject(session, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Enter task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = taskEndpoint.findByIdTask(session, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        projectTaskEndpoint.unbindTaskById(session, projectId, taskId);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
