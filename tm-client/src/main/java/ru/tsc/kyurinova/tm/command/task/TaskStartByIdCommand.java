package ru.tsc.kyurinova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

@Component
public class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-start-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start task by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        taskEndpoint.startByIdTask(sessionService.getSession(), id);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
