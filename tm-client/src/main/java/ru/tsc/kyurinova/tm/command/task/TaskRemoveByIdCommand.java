package ru.tsc.kyurinova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("[REMOVE TASK BY ID]");
        taskEndpoint.removeByIdTask(sessionService.getSession(), id);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
