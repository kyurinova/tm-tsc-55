package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;

@Component
public class DataJsonLoadJaxBCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-jaxb-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load json data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON JAXB LOAD]");
        adminDataEndpoint.dataJsonLoadJaxB(sessionService.getSession());
    }

}
