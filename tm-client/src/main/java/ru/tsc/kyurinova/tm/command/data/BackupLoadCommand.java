package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;

@Component
public class BackupLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "backup-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load backup from XML";
    }

    @Override
    @SneakyThrows
    public void execute() {
        adminDataEndpoint.dataBackupLoad(sessionService.getSession());
    }

}
