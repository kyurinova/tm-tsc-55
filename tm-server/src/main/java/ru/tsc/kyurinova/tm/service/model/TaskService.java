package ru.tsc.kyurinova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.model.ITaskRepository;
import ru.tsc.kyurinova.tm.api.repository.model.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.model.ITaskService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.model.TaskRepository;
import ru.tsc.kyurinova.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;


    public TaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
    }

    @Override
    public void addAll(@NotNull final List<Task> tasks) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (Task task : tasks) {
                taskRepository.add(task.getUser().getId(), task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }


    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final Task task = new Task();
        User user = userRepository.findById(userId);
        task.setUser(user);
        task.setName(name);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final Task task = new Task();
        User user = userRepository.findById(userId);
        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            return taskRepository.findByName(userId, name);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            Task Task = findById(userId, id);
            Task.setName(name);
            Task.setDescription(description);
            taskRepository.update(userId, Task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            Task Task = findByIndex(userId, index);
            Task.setName(name);
            Task.setDescription(description);
            taskRepository.update(userId, Task);
            entityManager.getTransaction().commit();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.startById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.startByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.startByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.finishById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.finishByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.finishByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.changeStatusById(userId, id, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.changeStatusByIndex(userId, index, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.changeStatusByName(userId, name, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task findByProjectAndTaskId(@Nullable final String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            return taskRepository.findByProjectAndTaskId(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.remove(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            return taskRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            return taskRepository.findAllUserId(userId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clearUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            return taskRepository.findById(userId, id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            return taskRepository.findByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            @NotNull final String id = taskRepository.findByIndex(userId, index).getId();
            entityManager.getTransaction().begin();
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            @Nullable final Task task = taskRepository.findById(userId, id);
            return task != null;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            @Nullable final Task task = taskRepository.findByIndex(userId, index);
            return task != null;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

}
