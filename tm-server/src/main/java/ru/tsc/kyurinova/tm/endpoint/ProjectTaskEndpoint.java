package ru.tsc.kyurinova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void bindTaskById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void unbindTaskById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @NotNull
    @WebMethod
    @SneakyThrows
    public void removeAllTaskByProjectId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().removeAllTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().removeById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    List<TaskDTO> findAllTaskByProjectId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(session.getUserId(), projectId);
    }
}
