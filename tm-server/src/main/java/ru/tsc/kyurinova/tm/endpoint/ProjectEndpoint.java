package ru.tsc.kyurinova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    ProjectDTO entity
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    List<ProjectDTO> findAllProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    @SneakyThrows
    public List<ProjectDTO> findAllProjectsSorted(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "sort", partName = "sort") final String sort
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll();
    }


    @Override
    @WebMethod
    @SneakyThrows
    public void clearProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable
    ProjectDTO findByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    ProjectDTO findByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createProjectDescr(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    ProjectDTO findByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void updateByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void updateByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void startByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void startByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void startByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void finishByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void finishByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void finishByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void changeStatusByIdProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void changeStatusByIndexProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void changeStatusByNameProject(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    ProjectDTO entity
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), entity);
    }


    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    List<ProjectDTO> findAllProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable
    ProjectDTO findByIdProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    ProjectDTO findByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByIdProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIdProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIndexProjectUserId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsByIndex(session.getUserId(), index);
    }
}
