package ru.tsc.kyurinova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;

@NoArgsConstructor
public class AbstractEndpoint {

    protected IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
